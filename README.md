Configuring inital DB


CREATING DB:
requirements: python3 , pip3, python-mysql-connector



```
cd scripts;
./run.sh
```

runs the following files sequentially:



script1.sql (note path to csv needs to be updated for user running script)
script2.sql
script3.sql
python3 update_course_offering_date.py  # with mysql-connector pip module installed 
python3 update_imd_band.py  # with mysql-connector pip module installed 
script.sql 