import os
from flask import Flask

class AppConfig:

    @staticmethod
    def db_config():
        config = {
            'user':os.environ["DB_USER"], 
            'password': os.environ["DB_PASSWORD"],
            'host':os.environ["DB_HOST"],
            'database':os.environ["DB_NAME"],
            'use_pure': True  # uses C extension
        }
        return config
