from api.models.courseInfo import CourseInfo
from models.studentUnregistration import StudentUnregistration
from models.assessments import Assessments
from models.courseOfferings import CourseOfferings
from models.studentAssements import StudentAssessments
from models.studentInfo import StudentInfo
from models.studentRegistration import StudentRegistration
from models.studentUnregistration import StudentUnregistration


class Controller:

    relation_2_class_map = {}
    joinable_tables_map = {}

    def __init__(self):

        self.relation_2_class_map[Assessments.__tablename__] = Assessments
        self.relation_2_class_map[CourseOfferings.__tablename__] = CourseOfferings
        self.relation_2_class_map[StudentAssessments.__tablename__] = StudentAssessments
        self.relation_2_class_map[StudentRegistration.__tablename__] = StudentInfo
        self.relation_2_class_map[
            StudentUnregistration.__tablename__
        ] = StudentUnregistration
        self.relation_2_class_map[StudentInfo.__tablename__] = StudentInfo
        self.relation_2_class_map[CourseInfo.__tablename__] = CourseInfo

        # self.relation_2_class_map[
        #     StudentRegistration.__tablename__
        # ] = StudentRegistration

        # JOINABLE TABLES
        self.joinable_tables_map[Assessments.__tablename__] = set(
            [StudentAssessments, CourseOfferings]
        )

        self.joinable_tables_map[CourseOfferings.__tablename__] = set(
            [
                StudentAssessments,
                Assessments,
                StudentInfo,
                StudentRegistration,
                StudentUnregistration,
            ]
        )  # TODO add course info

        self.joinable_tables_map[StudentAssessments.__tablename__] = set(
            [Assessments, StudentInfo]
        )

        self.joinable_tables_map[StudentInfo.__tablename__] = set(
            [CourseOfferings, StudentAssessments]
        )

    def relations_list(self):
        return list(self.relation_2_class_map.keys())
