import mysql.connector
import sys
sys.path.insert(0, "../..")
from utils.app_config import AppConfig

class DatabaseConnectionsQueue():
    class DatabaseConnectionNode():
        MAX_CONNECTION_TIME = 100  # in seconds

        def __init__(
            self,
        ) -> None:
            try:
                self.connection = mysql.connector.connect(**AppConfig.db_config())
                self.init_failed = False
            except Exception as err:

                print(f"ERROR: {err}")
                self.init_failed = True

        def _query_on_connection(self, query_string, query_vals, select=True):
            res = None

            try:
                try:
                    cursor = self.connection.cursor()
                except:
                    self.connection = mysql.connector.connect(**AppConfig.db_config())
                    cursor = self.connection.cursor()

                query_vals = tuple(query_vals)
                cursor.execute(query_string, query_vals)

                if select:
                    res = cursor.fetchall()
                else:
                    self.connection.commit()
            
            except Exception as err:
                print("ERROR: ", err)
                res = None

            finally:
                return res

        def __str__(self):
            return f"<DB Conn: time={self.sortable_value}, open={not self.connection.closed} > "

    def __init__(self, capacity):
        self.node = self.DatabaseConnectionNode()

    def _query(self, query_string, query_vals=[], select=True):
        return self.node._query_on_connection(query_string, query_vals, select)