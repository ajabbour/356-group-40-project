def abstractmethod(func):
    """Decorator that prevents implementation."""

    def wrap(*args, **kwargs):
        raise NotImplementedError("Cannot Call Abstract Method")

    return wrap

class AbstractBaseClass:
    __abstract__ = True

    # def __init__(self) -> None:
    #     raise Exception("Cannot Implement AbstractBaseClass")
