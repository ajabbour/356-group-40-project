from models.base_model import BaseModel


class StudentAssessments(BaseModel):

    __tablename__ = "studentAssessments"

    id_assessment = None
    id_student = None
    date_submitted = None
    is_banked = None
    score = None
    failed = None

    def __init__(
        self,
        id_student=None,
        date_submitted=None,
        is_banked=None,
        score=None,
        failed=None,
        skip_creation=False,
    ):
        self.id_student = id_student
        self.date_submitted = date_submitted
        self.is_banked = is_banked
        self.score = score
        self.failed = failed

        if not skip_creation:
            self._create_self(self)

    @staticmethod
    def _get_columns() -> list:
        return [
            "id_assessment",
            "id_student",
            "date_submitted",
            "is_banked",
            "score",
            "failed",
        ]

    def __repr__(self):
        return f"< StudentAssessments id_assessment :  {self.id_assessment},  id_student : {self.id_student}  >"

    def __str__(self):
        return f"< StudentAssessments id_assessment :  {self.id_assessment},  id_student : {self.id_student}  >"
