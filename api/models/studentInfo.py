from api.models import studentAssements
from models.base_model import BaseModel


class StudentInfo(BaseModel):

    __tablename__ = "studentInfo"

    id_student = None
    highest_education = None
    num_of_prev_attempts = None
    studied_credits = None
    course_offering_id = None
    age_range = None
    regionid = None
    education_rank = None
    gender = None
    imd_band = None

    def __init__(
        self,
        id_student = None,
        highest_education=None,
        num_of_prev_attempts=None,
        studied_credits=None,
        course_offering_id=None,
        age_range=None,
        regionid=None,
        education_rank=None,
        gender=None,
        imd_band=None,
        skip_creation=False,
    ):
        self.highest_education = highest_education
        self.num_of_prev_attempts = num_of_prev_attempts
        self.studied_credits = studied_credits
        self.course_offering_id = course_offering_id
        self.age_range = age_range
        self.regionid = regionid
        self.education_rank = education_rank
        self.gender = gender
        self.imd_band = imd_band

        if not skip_creation:
            self._create_self(self)

    @staticmethod
    def _get_columns() -> list:
        return [
            "id_student",
            "highest_education",
            "num_of_prev_attempts",
            "studied_credits",
            "course_offering_id",
            "age_range",
            "regionid",
            "education_rank",
            "gender",
            "imd_band",
        ]

    def __as_small_dict__(self):
        return {"id_student": self.id_student}

    def __repr__(self):
        return f"< StudentInfo id_student {self.id_student} >"

    def __str__(self):
        return f"< StudentInfo id_student {self.id_student} >"
