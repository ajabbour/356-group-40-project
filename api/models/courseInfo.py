from models.base_model import BaseModel


class CourseInfo(BaseModel):

    __tablename__ = "courseInfo"

    course_id = None
    course_code = None
    description = None
    year = None

    def __init__(
        self,
        course_code=None,
        description=None,
        skip_creation=False,
    ):
        self.course_code = course_code
        self.description = description

        if not skip_creation:
            self._create_self(self)

    @staticmethod
    def _get_columns() -> list:
        return [
            "course_id",
            "course_code",
            "description",
        ]

    def __as_small_dict__(self):
        return {
            "course_id": self.course_id,
        }

    def __repr__(self):
        return f"< CourseInfo course_id {self.course_id} >"

    def __str__(self):
        return f"< CourseInfo course_id {self.course_id} >"
