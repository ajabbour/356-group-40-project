from models.base_model import BaseModel


class StudentRegistration(BaseModel):

    __tablename__ = "studentRegistration"

    id_registration = None
    id_student = None
    date_registration = None
    course_offering_id = None

    def __init__(
        self,
        id_student=None,
        date_registration=None,
        course_offering_id=None,
        skip_creation=False,
    ):

        self.id_student = id_student
        self.date_registration = date_registration
        self.course_offering_id = course_offering_id

        if not skip_creation:
            self._create_self(self)

    @staticmethod
    def _get_columns() -> list:
        return [
            "id_registration",
            "id_student",
            "date_registration",
            "course_offering_id",
        ]

    def __as_small_dict__(self):
        return {"id_student": self.id_student}

    def __repr__(self):
        return f"< StudentRegistration  id_registration : {self.id_registration},  id_student : {self.id_student}>"

    def __str__(self):
        return f"< StudentRegistration  id_registration : {self.id_registration},  id_student : {self.id_student}>"
