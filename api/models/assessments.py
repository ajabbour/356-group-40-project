from models.base_model import BaseModel


class Assessments(BaseModel):

    __tablename__ = "assessments"

    course_offering_id = None
    id_assssment = None
    assessment_type = None
    date = None
    weight = None

    def __init__(
        self,
        course_offering_id=None,
        assessment_type=None,
        date=None,
        weight=None,
        skip_creation=False,
    ):
        self.course_offering_id = course_offering_id
        self.assessment_type = assessment_type
        self.date = date
        self.weight = weight

        if not skip_creation:
            print("\n\n\nSDFDDDDDDD")
            self._create_self(self)
    

    @staticmethod
    def clean_row(row):
        for k in row.keys():
            if row[k] == "":
                row[k] = None
            elif k == "id_assssment" or k == "date" or k == "weight":
                row[k] = int(row[k])

        return row 
            
    @staticmethod
    def _get_columns() -> list:
        return [
            "id_assssment",
            "assessment_type",
            "course_offering_id",
            "date",
            "weight",
        ]

    def __as_small_dict__(self):
        return {
            "course_offering_id": str(self.course_offering_id),
            "id_assssment": str(self.id_assssment),
        }

    def __repr__(self):
        return f"< Assessment course_offering_id {self.course_offering_id}, id_assssment {self.id_assssment} >"

    def __str__(self):
        return f"< Assessment course_offering_id {self.course_offering_id}, id_assssment {self.id_assssment} >"
