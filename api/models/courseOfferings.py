from models.base_model import BaseModel


class CourseOfferings(BaseModel):

    __tablename__ = "courseOfferings"

    course_offering_id = None
    course_offering_length_in_days = None
    semester = None
    year = None

    def __init__(
        self,
        course_offering_length_in_days=None,
        semester=None,
        year=None,
        skip_creation=False,
    ):
        self.course_offering_length_in_days = course_offering_length_in_days
        self.semester = semester
        self.year = year

        if not skip_creation:
            self._create_self(self)

    @staticmethod
    def clean_row(row):
        for k in row.keys():
            if row[k] == "":
                row[k] = None
            elif k == "course_offering_length_in_days" or k =="id_student" or \
                k == "is_banked" or k == "score" or k == "failed"  or k == "num_prev_attempts" \
                or k == "studied_credits" or k == "course_offering_id" or k == "regionid" \
                or k == "education_rank" or  k == "imd_band" or k == "id_registration" or k == "id_unregistration" \
                or k == "date_registration" or k  == "date_unregistration": 
                row[k] = int(row[k])

        return row 

    @staticmethod
    def _get_columns() -> list:
        return [
            "course_offering_length_in_days",
            "course_offering_id",
            "semester",
            "year",
        ]

    def __as_small_dict__(self):
        return {
            "course_offering_id": self.course_offering_id,
        }

    def __repr__(self):
        return f"< CourseOfferings course_offering_id {self.course_offering_id} >"

    def __str__(self):
        return f"< CourseOfferings course_offering_id {self.course_offering_id} >"
