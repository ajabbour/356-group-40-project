from models.base_model import BaseModel


class StudentUnregistration(BaseModel):

    __tablename__ = "studentUnregistration"

    id_unregistration = None
    id_student = None
    date_unregistration = None
    course_offering_id = None
    id_registration = None

    def __init__(
        self,
        id_student=None,
        date_unregistration=None,
        course_offering_id=None,
        id_registration=None,
        skip_creation=False,
    ):

        self.id_student = id_student
        self.date_unregistration = date_unregistration
        self.id_registration = id_registration
        self.course_offering_id = course_offering_id

        if not skip_creation:
            self._create_self(self)

    @staticmethod
    def _get_columns() -> list:
        return [
            "id_unregistration",
            "id_student",
            "date_unregistration",
            "course_offering_id",
            "id_registration",
        ]

    def __as_small_dict__(self):
        return {"id_student": self.id_student}

    def __repr__(self):
        return f"< studentUnregistration  id_unregistration : {self.id_registration},  id_student : {self.id_student}>"

    def __str__(self):
        return f"< studentUnregistration  id_unregistration : {self.id_registration},  id_student : {self.id_student}>"
