from utils import *
from parser import *

options_switchcase = {
	"insert" : insert_base_func,
	"modify" : modify_base_func,
	"delete-from" : delete_base_func,
	"get-from" : get_base_func
}

if __name__ == "__main__":
	# message at the start:
	
	print(
	'''
	Welcome to our ECE 356 final project client!

	These are our base commands. For each of them, you must start by specifying the --table:
	insert      - inserts an entry into the table
	modify      - modifies an entry in a table by primary key
	delete-from - deletes a row (or mulitple rows) from the table
	get-from    - fetches a set of rows from a table in the DB

	These are the more 'user friendly' commands:
	create-new-student
	create-new-course
	create-new-vle
	create-new-assessment
	assign-assessment-to-student
	unregister-student
	get-all-courses
	get-course-offerings-by-year

	''')


	while True:
		command = input("Command: ").split()
		if len(command) == 0: continue

		if command[0] not in options_switchcase:
			print(f"Error: command not in options. Options are:\n {list(options_switchcase.keys())}")
			continue

		if command[0] == "exit":
			exit("Terminating...")

		# pick the appropirate function given command[0]
		options_switchcase[command[0]](command)
		