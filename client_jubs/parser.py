import argparse

# the base table specifications - for the table at hand
tables=[
        "courseInfo",
        "assessments",
        "studentAssessments",
        "courseOfferings",
        "studentInfo",
        "region",
        "educationLevel",
        "vle",
        "studentUnregistration",
        "studentRegistration",
        "studentVle"
    ]

# ========== INSERT ENTRY SECTION ==========
insert_entry_parser = argparse.ArgumentParser()
insert_entry_parser.add_argument(
    "--table",
    dest="table",
    choices=tables,
    help="specify the table to perform CRUD on",
    required=True
)

# ========== MODIFY TABLE SECTION ==========
modify_table_parser = argparse.ArgumentParser()
modify_table_parser.add_argument(
    "--table",
    dest="table",
    choices=tables,
    help="specify the table to perform CRUD on",
    required=True
)

# ========== DELETE SECTION ==========
delete_rows_parser = argparse.ArgumentParser()
delete_rows_parser.add_argument(
    "--table",
    dest="table",
    choices=tables,
    help="specify the table to perform CRUD on",
    required=True
)

# ========== GET SECTION ==========
get_rows_parser = argparse.ArgumentParser()
get_rows_parser.add_argument(
    "--table",
    dest="table",
    choices=tables,
    help="specify the table to perform CRUD on",
    required=True
)