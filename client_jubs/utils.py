from get_attributes import *
from parser import *

def handle_collecting_insert_args(attributes, new_row):
	# the first element in attributes is the PK
	for i in range(len(attributes)):
		if i == 0:
			new_row[attributes[i]] = input(f"specify {attributes[i]} (this is a PK - it is required): ")
			if new_row[attributes[i]] == '': return -1
			continue
		new_row[attributes[i]] = input(f"specify {attributes[i]} (blank space for none): ")
	return 0

def handle_collecting_modify_args(attributes, new_row):
	# the first element in attributes is the PK
	for i in range(len(attributes)):
		if i == 0:
			new_row[attributes[i]] = input(f"specify {attributes[i]} (this is a PK - it is required): ")
			if new_row[attributes[i]] == '': return -1
			continue
		new_row[attributes[i]] = input(f"specify {attributes[i]} (blank space for no change): ")
	return 0

def handle_filtering_on_table(attributes):
	print(f"applying filters for {attributes}")
	filters = list()
	int_operators = ["=", ">", "<", ">=", "<=", "<>"]
	string_operators = ["LIKE", "="]
	bool_operators = ["="]
	should_filter = input(f"Would you like to apply a filter for any of `{attributes}`? \n 'y' or 'yes': ") in {"y", "yes"}
	while (should_filter):
		filtering_attribute = input(f"Which would you like to filter on out of {attributes}: ")
		while (filtering_attribute not in attributes):
			filtering_attribute = input(f"Which would you like to filter on out of {attributes}: ")
		
		print(f"Filtering on {filtering_attribute}. The type is {attribute_type[filtering_attribute]}")

		if attribute_type[filtering_attribute] == int:
			operator = input(f"Enter the operator (in {int_operators}): ")
			while operator not in int_operators:
				operator = input(f"Enter the operator (in {int_operators}): ")
			value = int(input("Enter the filtering value (must be int): "))

		if attribute_type[filtering_attribute] == str:
			print("note: wildcard matching works on 'LIKE' operator")
			operator = input(f"Pick an operator ({string_operators}): ")
			while operator not in string_operators:
				operator = input(f"Pick an operator ({string_operators}): ")
			value = input("Enter the filtering value: ")

		if attribute_type[filtering_attribute] == bool:
			operator = input(f"Pick an operator ({bool_operators}): ")
			while operator not in bool_operators:
				operator = input(f"Pick an operator ({bool_operators}): ")

			value = input("Enter the filtering value (1 or 0): ")
			while value not in {"0", "1"}:
				value = input("Enter the filtering value (1 or 0): ")
		filters.append({
			"attribute" : filtering_attribute,
			"value" : value,
			"operator" : operator	
		})

		should_filter = input(f"Would you like to apply another filter for any of `{attributes}`? \n 'y' or 'yes': ") in {"y", "yes"}

def insert_base_func(command):
	insert_entry_args = insert_entry_parser.parse_args(command[1:])
	new_row = dict()

	# branch based on the table 
	if handle_collecting_insert_args(
		attributes=get_attributes_from_tablename[insert_entry_args.table],
		new_row=new_row
	) == -1: return
	print(f"INSERTING row: \n {new_row} \n into table {insert_entry_args.table}")


def modify_base_func(command):
	modify_table_args = modify_table_parser.parse_args(command[1:])
	new_row = dict()

	# branch based on the table 
	if handle_collecting_modify_args(
		attributes=get_attributes_from_tablename[modify_table_args.table],
		new_row=new_row
	) == -1: return
	print(f"INSERTING row: \n {new_row} \n into table {modify_table_args.table}")


def delete_base_func(command):
	delete_rows_args = delete_rows_parser.parse_args(command[1:])
	filters = list()

	# branch based on the table
	handle_filtering_on_table(
		attributes=get_attributes_from_tablename[delete_rows_args.table],
		filters=filters
	)

	print(f"DELETING rows from {delete_rows_args.table}: \n with filters: {filters} \n")

def get_base_func(command):
	get_rows_args = get_rows_parser.parse_args(command[1:])
	filters = list()

	# apply filters
	handle_filtering_on_table(
		attributes=get_attributes_from_tablename[get_rows_args.table],
		filters=filters
	)

	print(f"GETTING rows from {get_rows_args.table}: \n with filters: {filters} \n")

