# the 0th index in every one of these should be the PK
get_attributes_from_tablename = {
    "courseInfo" : [ 
        "course_id",
        "course_code",
        "description" 
        ],
    "assessments" : [ 
        "id_assessment",
        "course_offering_id",
        "date",
        "weight",
        "assessment_type"
        ],
    "studentAssessments" : [
        "id_assessment",
        "id_student",
        "date_submitted",
        "is_banked",
        "score",
        "failed"
        ],
    "courseOfferings" : [
        "course_offering_id",
        "course_offering_length_in_days",
        "year",
        "semester"
        ],
    "studentInfo" : [
        "id_student",
        "highest_education",
        "num_of_prev_attempts",
        "studied_credits",
        "course_offering_id",
        "age_range",
        "regionid",
        "education_rank",
        "gender",
        "imd_band"
        ],
    "region" : [
        "regionid",
        "name"
    ],
    "educationLevel" : [
        "education_rank",
        "education_level"
    ],
    "vle" : [
        "id_site",
        "activity_type",
        "week_from",
        "week_to",
        "course_offering_id"
    ],
    "studentUnregistration" : [
        "id_unregistration",
        "id_student",
        "date_unregistration",
        "course_offering_id",
        "id_registration"
    ],
    "studentRegistration" : [
        "id_registration",
        "id_student",
        "date_registration",
        "date_unregistration",
        "course_offering_id"
    ],
    "studentVle" : [
        "id_student",
        "id_site",
        "date",
        "interactions",
        "course_offering_id",
        "id"   
    ]
}
attribute_type = {
    "course_id": int,
    "course_code": str,
    "description" : str,
    "id_assessment": int,
    "course_offering_id": int,
    "date": int,
    "weight": int,
    "assessment_type": str, # enum
    "id_assessment" : int,
    "id_student" : int,
    "date_submitted" : int,
    "is_banked" : bool,
    "score": int,
    "failed": bool,
    "course_offering_length_in_days": int,
    "year": int,
    "semester": str, # enum
    "highest_education": str,
    "num_of_prev_attempts": int,
    "studied_credits": int,
    "age_range": str, # enum
    "regionid": int,
    "education_rank": int,
    "gender": str, # enum
    "imd_band": int,
    "name": str,
    "education_level": str,
    "id_site": int,
    "activity_type": str,
    "week_from": int,
    "week_to": int,
    "id_unregistration": int,
    "id_student": int,
    "date_unregistration": int,
    "id_registration": int,
    "date_registration": int,
    "interactions": int,
    "id": int
}