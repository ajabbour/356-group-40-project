-- normalize student registration tables

update studentRegistration 
set date_unregistration = NULL where date_unregistration = 0;

update studentRegistration 
set date_registration = NULL where date_registration = 0;


create table studentUnregistration(
    id_unregistration int AUTO_INCREMENT, 
    id_student int NOT NULL,
    date_unregistration int NOT NULL,
    course_offering_id int,
    id_registration int,
    PRIMARY key (id_unregistration),
    constraint fk_studentid_unregistration FOREIGN key (id_student) REFERENCES studentInfo(id_student),
    constraint fk_courseofferingid_unregistration FOREIGN key (course_offering_id) REFERENCES courseOfferings(course_offering_id), 
    constraint fk_registrationid_unregistration FOREIGN key (id_registration) REFERENCES studentRegistration(id_registration)
);


-- Populate studentUnregistration
insert into studentUnregistration(id_student, date_unregistration, course_offering_id, id_registration)
select id_student, date_unregistration, course_offering_id, id_registration from studentRegistration
    where  date_unregistration is not NULL;



-- CHANGE studentInfo
ALTER TABLE studentInfo 
    rename column imd_band to imd_band_o;

ALTER TABLE studentInfo
    add column imd_band int;


alter table courseOfferings 
    add column year int not null,
    add column semester ENUM('Fall', 'Winter') not null;

