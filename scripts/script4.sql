-- AFTER RUNNING PYTHON!

ALTER TABLE studentInfo
    drop column imd_band_o;

UPDATE studentInfo SET 
    imd_band = NULL where imd_band = -1;


alter table assessments
    rename column date to date_o,
    rename column weight to weight_o,
    add column date int,
    add column weight int;


update assessments 
    set date_o = "-9999.9" 
    where date_o = "";



--  cant have a negative score likely wont happen 
update assessments 
    set weight_o =  "-9999.9" 
    where weight_o = "";


update assessments 
    set date = CAST(date_o AS DECIMAL),
    weight = CAST(weight_o AS DECIMAL)
    where 1;

update assessments
    set date = NULL 
    where date = -9999.9;

update assessments
    set weight = NULL 
    where weight = -9999.9;


alter table assessments
    drop column date_o, 
    drop column weight_o;



drop table if exists courseInfo;

create table courseInfo (
    course_id int AUTO_INCREMENT,
    course_code CHAR(3),
    description varchar(500),
    PRIMARY key (course_id)
    -- constraint fk_courseid_courseinfo 
    --     FOREIGN key (course_id) REFERENCES courseOfferings(course_offering_id)
);


insert into courseInfo (course_code)
select distinct code_module from courseOfferings;


alter table courseOfferings 
    add column course_id INT,
    add constraint foreign key(course_id) references courseInfo(course_id);


UPDATE courseOfferings LEFT JOIN courseInfo ON courseOfferings.code_module = courseInfo.course_code
SET courseOfferings.course_id = courseInfo.course_id;


alter table courseOfferings
    drop column code_module, 
    drop column code_presentation,
    rename column module_presentation_length to course_offering_length_in_days;

alter table assessments 
    rename column assessment_type to assessment_type_o,
    add column assessment_type ENUM("TMA", "Exam", "CMA");

update assessments
    set assessment_type = assessment_type_o
    where 1;

alter table assessments
    drop column assessment_type_o;


alter table studentInfo
    drop column highest_education;

alter table studentRegistration
    drop column date_unregistration;
