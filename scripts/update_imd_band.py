import mysql.connector

cnx = mysql.connector.connect(host='localhost', user='snvercil', database='Education', password="pass")
cursor = cnx.cursor()

cursor.execute("SELECT * FROM studentInfo;")

c = cursor.fetchall()
for row in c:
    d = row[2]

    if d == "" or d is None:
        d = -1
    elif d == "0-10%":
        d = 10
    elif d == "10-20":
        d = 20
    elif d == "20-30%":
        d = 30
    elif d == "30-40%":
        d = 40
    elif d == "40-50%":
        d = 50
    elif d == "50-60%":
        d = 60
    elif d == "60-70%":
        d = 70
    elif d == "70-80%":
        d = 80
    elif d == "80-90%":
        d = 90
    elif d == "90-100%":
        d = 100

    s = f"update studentInfo set imd_band = {d} where id_student = {row[0]}"
    print(s)
    cursor.execute(s)

# Make sure data is committed to the database
cnx.commit()

cursor.close()
cnx.close()
