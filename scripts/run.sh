#!/bin/bash

# ** README **
# Requirements:
# 1. mysql-connector pip module installed
# 2. local path to loading data files updated in script1
# 3. input your database name for the db_name variable
db_name="Education"
user="snvercil"
password="pass"

mysql -u $user --password=$password --verbose --verbose $db_name < "script1.sql"
mysql -u $user --password=$password --verbose --verbose $db_name < "script2.sql"
mysql -u $user --password=$password --verbose --verbose $db_name < "script3.sql"

# # # tested up to there ^
python3 update_course_offering_date.py
python3 update_imd_band.py

mysql -u $user --password=$password --verbose --verbose $db_name < "script4.sql"

